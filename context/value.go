package context

import (
	"context"
	"github.com/spf13/cast"
	"time"
)

type Value interface {
	Values() map[any]any
	WithValue(key, value any)

	WithUserID(string)
	ID() string
	UserID() string

	ValueString(key any) string
	ValueBool(key any) bool
	ValueFloat32(key any) float32
	ValueFloat64(key any) float64
	ValueInt(key any) int
	ValueInt8(key any) int8
	ValueInt16(key any) int16
	ValueInt32(key any) int32
	ValueInt64(key any) int64
	ValueUint(key any) uint
	ValueUint8(key any) uint8
	ValueUint16(key any) uint16
	ValueUint32(key any) uint32
	ValueUint64(key any) uint64
	ValueStringMapString(key any) map[string]string
	ValueDuration(key any) time.Duration
}

func (l local) ID() string {
	return l.ValueString(keyRequestID)
}

func (l local) WithUserID(userID string) {
	context.WithValue(l.base, keyUserID, userID)
}

func (l local) UserID() string {
	return l.ValueString(keyUserID)
}

func (l local) Value(key any) any {
	return l.base.Value(key)
}

func (l local) ValueString(key any) string {
	return cast.ToString(l.base.Value(key))
}

func (l local) ValueBool(key any) bool {
	return cast.ToBool(l.base.Value(key))
}

func (l local) ValueFloat32(key any) float32 {
	return cast.ToFloat32(l.base.Value(key))
}

func (l local) ValueFloat64(key any) float64 {
	return cast.ToFloat64(l.base.Value(key))
}

func (l local) ValueInt(key any) int {
	return cast.ToInt(l.base.Value(key))
}

func (l local) ValueInt8(key any) int8 {
	return cast.ToInt8(l.base.Value(key))
}

func (l local) ValueInt16(key any) int16 {
	return cast.ToInt16(l.base.Value(key))
}

func (l local) ValueInt32(key any) int32 {
	return cast.ToInt32(l.base.Value(key))
}

func (l local) ValueInt64(key any) int64 {
	return cast.ToInt64(l.base.Value(key))
}

func (l local) ValueUint(key any) uint {
	return cast.ToUint(l.base.Value(key))
}

func (l local) ValueUint8(key any) uint8 {
	return cast.ToUint8(l.base.Value(key))
}

func (l local) ValueUint16(key any) uint16 {
	return cast.ToUint16(l.base.Value(key))
}

func (l local) ValueUint32(key any) uint32 {
	return cast.ToUint32(l.base.Value(key))
}

func (l local) ValueUint64(key any) uint64 {
	return cast.ToUint64(l.base.Value(key))
}

func (l local) ValueStringMapString(key any) map[string]string {
	return cast.ToStringMapString(l.base.Value(key))
}

func (l local) ValueDuration(key any) time.Duration {
	return cast.ToDuration(l.base.Value(key))
}

func (l local) Values() map[any]any {
	panic("implement me")
}

func (l *local) WithValue(key, value any) {
	l.base = context.WithValue(l.base, key, value)
}

func (l *local) WithTimeout(timeout time.Duration) {
	l.base, l.cancelFunc = context.WithTimeout(l.base, timeout)
}

func (l *local) WithDeadline(d time.Time) {
	l.base, l.cancelFunc = context.WithDeadline(l.base, d)
}
