package context

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"google.golang.org/grpc/metadata"
	"log"
	"os"
	"time"
)

func init() {
	keyRequestID = "id"
	if value := os.Getenv("CONTEXT_KEY_REQUEST_ID"); len(value) > 0 {
		keyRequestID = value
	}

	keyUserID = "userId"
	if value := os.Getenv("CONTEXT_KEY_USER_ID"); len(value) > 0 {
		keyUserID = value
	}
}

type Context interface {
	context.Context

	// expanding for the basic context
	Value

	WithDeadline(d time.Time)
	WithTimeout(timeout time.Duration)
	Cancel()
}

var (
	keyRequestID string
	keyUserID    string
)

type local struct {
	base       context.Context
	cancelFunc context.CancelFunc
}

func (l local) Deadline() (deadline time.Time, ok bool) {
	return l.base.Deadline()
}

func (l local) Done() <-chan struct{} {
	return l.base.Done()
}

func (l local) Err() error {
	return l.base.Err()
}

func (l *local) Cancel() {
	if l.cancelFunc != nil {
		l.cancelFunc()
	}
}

func Copy() Context {
	panic("implement me")
}

var cancelFunc = func() { log.Print("context has no data to clear") }

func Empty() Context {
	ctx := &local{
		base:       context.Background(),
		cancelFunc: cancelFunc,
	}

	ctx.WithValue(keyRequestID, uuid.New())
	ctx.WithValue(keyUserID, uuid.Nil)

	return ctx
}

type option interface {
	context.Context | gin.Context | *gin.Context | metadata.MD | *metadata.MD
}

func New(option option) Context {
	ctx := &local{
		base:       context.Background(),
		cancelFunc: cancelFunc,
	}
	switch baseCtx := option.(type) {
	case context.Context:
		ctx.base = baseCtx
	case gin.Context:
		ctx.base = baseCtx.Request.Context()
		for key, value := range baseCtx.Keys {
			ctx.WithValue(key, value)
		}

	case *gin.Context:
		ctx.base = baseCtx.Request.Context()
		for key, value := range baseCtx.Keys {
			ctx.WithValue(key, value)
		}
	case metadata.MD:
		metadata.NewIncomingContext(ctx.base, baseCtx)
	case *metadata.MD:
		metadata.NewIncomingContext(ctx.base, *baseCtx)
	}

	return ctx
}
